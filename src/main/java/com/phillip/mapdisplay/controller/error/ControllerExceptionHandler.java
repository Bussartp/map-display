package com.phillip.mapdisplay.controller.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.NoSuchElementException;
import java.util.UUID;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public ControllerExceptionHandler() {
        super();
    }

    // 400
    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleBadRequest(final ConstraintViolationException ex, final WebRequest request) {
        return handleExceptionInternal(ex, "", logError(ex, request, HttpStatus.BAD_REQUEST),
                HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({NoSuchElementException.class})
    public ResponseEntity<Object> handleBadRequest(final NoSuchElementException ex, final WebRequest request) {
        return handleExceptionInternal(ex, "", logError(ex, request, HttpStatus.BAD_REQUEST),
                HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({ValidationException.class})
    public ResponseEntity<Object> handleBadRequest(final ValidationException ex, final WebRequest request) {
        return handleExceptionInternal(ex, "", logError(ex, request, HttpStatus.BAD_REQUEST),
                HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status,
            final WebRequest request) {
        return handleExceptionInternal(null, "", logError(ex, request, HttpStatus.BAD_REQUEST),
                HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status,
            final WebRequest request) {
        return handleExceptionInternal(null, "", logError(ex, request, HttpStatus.BAD_REQUEST),
                HttpStatus.BAD_REQUEST, request);
    }

    // 500
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<Object> handleInternal(final Exception ex, final WebRequest request) {
        logger.error("Internal Server Error:", ex);
        return handleExceptionInternal(ex, "", logError(ex, request, HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    private HttpHeaders logError(final Exception ex, final WebRequest request, HttpStatus statusCode) {
        UUID uuid = UUID.randomUUID();
        StringBuilder message = new StringBuilder(100);

        message.append("Error caught by @ControllerAdvice.  Generated UUID:").append(uuid)
                .append(", WebRequest: ").append(request)
                .append(", Returning status code ").append(statusCode)
                .append(", Exception info: ").append(ex.getMessage());

        logger.error(message.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Error-Code", uuid.toString());

        return headers;
    }

}
