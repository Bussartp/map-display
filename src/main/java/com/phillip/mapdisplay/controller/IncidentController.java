package com.phillip.mapdisplay.controller;

import com.phillip.mapdisplay.utils.DataWriterReader;
import com.phillip.models.Incident;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("incident/")
@RestController
public class IncidentController {
    @GetMapping()
    public ResponseEntity getDisplay() {
        DataWriterReader dataWriterReader = new DataWriterReader();
        Incident incident = new Incident();
        incident = (Incident) dataWriterReader.readFromFile("F01705150050", incident.getClass());
        return ResponseEntity.ok(incident);
    }

}
