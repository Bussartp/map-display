package com.phillip.mapdisplay.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phillip.mapdisplay.service.DisplayService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequestMapping("display/")
@RestController
public class DisplayController {
    @Autowired
    DisplayService displayService;
    ObjectMapper mapper = new ObjectMapper();


    @GetMapping()
    public void getDisplay(HttpServletResponse response) throws IOException {
//
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("DisplayIncident");
//        File file = new File(getClass().getClassLoader().getResource("static/DisplayIncident.html").toURI());
//        String page = mapper.readValue(file, String.);

        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        IOUtils.copy(displayService.getImage(-122.4241, 37.78), response.getOutputStream());
    }

}
