package com.phillip.mapdisplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class MapDisplayApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapDisplayApplication.class, args);
    }

    @GetMapping("/")
    public String index() {
        return "Greetings from Map Display!";
    }
}
