package com.phillip.mapdisplay.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class DataWriterReader {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    ObjectMapper mapper = new ObjectMapper();


    public Object readFromFile(String fileName, Class<?> type) {
        log.info("Reading " + fileName + " from a file.");
        try {
            File file = new File(getClass().getClassLoader().getResource("examples/json/" + fileName + ".json").toURI());
            log.info("Found File: " + file.getAbsolutePath());
            return mapper.readValue(file, type);
        } catch (IOException | URISyntaxException e) {
            log.error(e.toString());
        }
        return null;
    }
}
