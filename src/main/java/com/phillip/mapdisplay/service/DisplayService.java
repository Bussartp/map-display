package com.phillip.mapdisplay.service;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;

@Service
public class DisplayService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    CloseableHttpClient closeableHttpClient;
    @Value("${apikey}")
    String apiKey;

    public DisplayService() {
        this.closeableHttpClient = HttpClientBuilder.create().build();
    }

    public InputStream getImage(Double longitude, double latitude) {
        CloseableHttpResponse httpResponse = null;
        String customURL = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/" + longitude.toString() + "," + latitude + ",10,0,0/600x600?access_token=" + apiKey;

        try {
            URL url = new URL(customURL);
            BufferedImage img = ImageIO.read(url);
            ImageIO.write(img, "png", new File(getClass().getClassLoader().getResource("images/temp.png").toURI()));

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(img, "png", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());

            return is;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }
}
