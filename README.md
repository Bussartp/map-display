
# springboot-sample-app

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- Access Token from mapbox.com.


## Building

First open upt the /resources/application.properties and set @@API_KEY@@ with your API access token from your mapbox.com account.

```shell
mvn clean install
```

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `de.codecentric.springbootsample.Application` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Testing

Open your favorite browser and go to:
```
http://localhost:8080/display/
```
This will return an image of San Fransisco using the mapbox.com API.
```
http://localhost:8080/incident/
```
This return back the json value of a saved incident report.


# Requirements README.md from project

```
Background
==========
Enrich 911 emergency incident data to provide better analytics for a fire department.

Task
----
Given an incident data, enrich it and then display the location and data on a map for easy validation. Try to utilize best practices where possible given available time. 

Enrichments
-----------
* Weather at the time of the incident (use a weather service of your choice, but https://dev.meteostat.net/ does include free historical queries).

Notes
-----
* Example incidents are provided in the data folder.
* We will test the project with an arbitrary incident file that is also from Richmond, VA and in the same format.
* It would be sufficient for the app to only handle one CAD file at a time.
* The incident location and attributes should be displayed on a map in the browser.
* You can enrich the incident and get it on a map however you wish.
* We would like for you to spend up to 4 hours. It is okay if you spend less time or more time so long as you return the project withing 24 hours of receiving it.
* Use technology stack and approach of your choice.

Deliverable
-----------
* Link to a Github repository with your commits as you originally made them. Do not squash them or just have a single commit. 
* There should be a README in the repo with the following section:
    * Steps to install and run your app. Assume the user will be on OSX but if you do not have access to OSX machine, provide needed steps to run your app on any other OS.
    * What improvements would you make or best practices would you utilize if you had double the time?
    * About how much time did you spend on the project?
* If you completed the project, add a few screenshots to the repo that show the working version as running on your machine.
```